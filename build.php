<?php

if (!file_exists('build')) {
    mkdir('build', 0777, true);
}
chdir('build');

if (!file_exists('box.phar')) {
    $box_installer = '/tmp/box_installer.php';

    file_put_contents($box_installer, file_get_contents('https://box-project.github.io/box2/installer.php'));
    
    include $box_installer;
    
    unlink($box_installer);
}

passthru("php box.phar build -c ../box.json", $return_var);

if ($return_var == 0) {
    echo 'Finished building phar'.PHP_EOL;
}
