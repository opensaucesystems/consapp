# Consapp

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Total Downloads][ico-downloads]][link-downloads]

**Note:** This is the core code that you can use to build an application from. To build an application, please visit [Consapp repository](https://bitbucket.org/opensaucesystems/consapp)

Consapp is a framework to build console applictions. It uses [Dice](https://r.je/dice.html) for dependency injection, [Symfony Console component](http://symfony.com/doc/current/components/console/introduction.html) to create the console commands and [Redbean](http://redbeanphp.com/index.php) for database interaction.

It is also can be used with [Box](http://box-project.github.io/box2/) to make a Phar.

## Install

Via Composer

``` bash
$ composer create-project consapp/consapp ./
```

## Usage

> TODO

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Security

If you discover any security related issues, please email ashley@opensauce.systems instead of using the issue tracker.

## Credits

- [Ashley Hood][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/consapp/consapp.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/consapp/consapp.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/consapp/consapp
[link-downloads]: https://packagist.org/packages/consapp/consapp
[link-author]: https://opensauce.systems
[link-contributors]: ./contributors
