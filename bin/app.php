#!/usr/bin/env php
<?php

/**
 * Register The Auto Loader.
 */
require __DIR__.'/../vendor/autoload.php';

$app = require_once __DIR__.'/../bootstrap/app.php';

// Additional Dice dependency injection container rules
$rules = [
    
];

foreach ($rules as $key => $rule) {
    $app->dice->addRule($key, $rule);
}

$app->registerCommands();

$app->get(Symfony\Component\Console\Application::class)->setName('consapp');
$app->get(Symfony\Component\Console\Application::class)->setVersion('0.0.7');

$app->run();
