<?php

/**
 * Create The Application.
 */
if (!empty(Phar::running())) {
    $basePath = Phar::running();
} else {
    $basePath = realpath(__DIR__.'/../');
}

$dice = new \Dice\Dice();

$rule = [
    'shared'          => true,
    'constructParams' => [$dice, $basePath],
];

$dice->addRule(Consapp\Application::class, $rule);

$app = $dice->create(Consapp\Application::class);

return $app;
