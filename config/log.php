<?php

return [
    'version' => 1,

    'handlers' => [
        'info_file_handler' => [
            'class' => 'Monolog\Handler\StreamHandler',
            'level' => 'INFO',
            'stream' => 'info.log'
        ],

        'error_file_handler' => [
            'class' => 'Monolog\Handler\StreamHandler',
            'level' => 'ERROR',
            'stream' => 'error.log',
        ],

        'error_slack_handler' => [
            'class'    => 'Monolog\Handler\SlackHandler',
            'level'    => 'ERROR',
            'token'    => 'xxxxxxxxxxxxxxxxxx',
            'channel'  => '#errors',
            'username' => 'error-bot',
        ],
    ],
    'processors' => [
        'mem_usage_processor' => [
            'class' => 'Monolog\Processor\MemoryUsageProcessor'
        ],
        'mem_peak_usage_processor' => [
            'class' => 'Monolog\Processor\MemoryPeakUsageProcessor'
        ],
    ],
    'loggers' => [
        'default_logger' => [
            'handlers' => ['info_file_handler', 'error_file_handler', 'error_slack_handler'],
            'processors' => ['mem_usage_processor', 'mem_peak_usage_processor'],
        ],
        'file_logger' => [
            'handlers' => ['info_file_handler', 'error_file_handler'],
            'processors' => ['mem_usage_processor', 'mem_peak_usage_processor'],
        ],
        'slack_logger' => [
            'handlers' => ['error_slack_handler'],
            'processors' => ['mem_usage_processor', 'mem_peak_usage_processor']
        ]
    ]
];
