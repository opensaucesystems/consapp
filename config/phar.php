<?php

return [

    /*
    | Urls for updating phar
    |
    */

    'urls' => [
        'phar'    => 'https://example.com/repofo.phar',
        'version' => 'https://example.com/repofo.version',
    ],

];
