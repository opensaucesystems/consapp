<?php

return [

    /*
    | Set model namespace
    |
    */

    'model' => '\App\Models',

    /*
    | The commands provided by application.
    |
    */

    'commands' => [
        \App\Console\Commands\GreetCommand::class,
        \App\Console\Commands\LoopCommand::class,
    ],

];
