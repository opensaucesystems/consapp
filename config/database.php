<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work.
    |
    */

    //'default' => env('DB_CONNECTION', 'mysql'),
    'default' => 'sqlite',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => 'database.sqlite',
        ],

        // 'mysql' => [
        //     'driver'    => 'mysql',
        //     'host'      => env('DB_HOST', 'localhost'),
        //     'database'  => env('DB_DATABASE', 'forge'),
        //     'username'  => env('DB_USERNAME', 'forge'),
        //     'password'  => env('DB_PASSWORD', ''),
        //     'charset'   => 'utf8',
        //     'collation' => 'utf8_unicode_ci',
        //     'prefix'    => '',
        //     'strict'    => false,
        // ],

    ],

];
