<?php

namespace App\Console\Commands;

use DateTime;
use Wrep\Daemonizable\Command\EndlessCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoopCommand extends EndlessCommand
{
    /**
     * @see Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this
            ->setName('demo:loop')
            ->setDescription('Loop until killed')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Who do you want to tell the time to?'
            )
            ->addOption(
                'yell',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            )
            // How often to run loop in seconds
            ->setTimeout(1);
        ;
    }

    /**
     * Initializes the command just after the input has been validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        if ($name) {
            $this->text = 'Hello '.$name.'. ';
        } else {
            $this->text = '';
        }

        $this->date = new DateTime();
    }

    /**
     * @see Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->date->setTimestamp(time());

        $text = $this->text.'The time is: '.$this->date->format('H:i:s');

        if ($input->getOption('yell')) {
            $text = strtoupper($text);
        }

        $output->writeln($text);

        $this->throwExceptionOnShutdown();
    }

    /**
     * Called before each iteration
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function startIteration(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start loop');
        parent::startIteration($input, $output);
    }

    /**
     * Called after each iteration
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function finishIteration(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Finished loop');
        parent::finishIteration($input, $output);
    }

    /**
    * Called on shutdown after the last iteration finished.
    *
    * Use this to do some cleanup, but keep it fast. If you take too long and we must
    * exit because of a signal changes are the process will be killed! It's the counterpart
    * of initialize().
    *
    * @param InputInterface  $input  An InputInterface instance
    * @param OutputInterface $output An OutputInterface instance, will be a NullOutput if the verbose is not set
    */
    protected function finalize(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Caught shutdown');

        parent::finalize($input, $output);
    }
}
